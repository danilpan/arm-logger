package logger

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"maps"
	"os"
	"runtime"
	"runtime/debug"
	"strings"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/danilpan/arm-logger/masker"
	"gitlab.com/danilpan/arm-logger/structs"
	loggerContext "gitlab.com/danilpan/arm-logger/structs/context"
	"gitlab.com/danilpan/arm-logger/structs/types"
	"gitlab.com/danilpan/arm-logger/types/field"
)

type logmode string

const (
	developMode    logmode = "develop"
	productionMode logmode = "production"
)

const (
	defaultSkipLevel = 2
)

var headersLogList = map[string]struct{}{
	"Accept":          {},
	"Accept-Encoding": {},
	"Accept-Language": {},
	"Forwarded":       {},
	"User-Agent":      {},
	"X-Request-Id":    {},
}

type Logger struct {
	skipLevel  int
	basic      structs.LogContent
	sanitizer  masker.LogMasker
	lg         *zerolog.Logger
	mode       logmode
	logHeaders map[string]struct{}
}

func NewLoggerWithOutput(output io.Writer) (*Logger, error) {
	if output == nil {
		return nil, errors.New("empty output")
	}
	lg := zerolog.New(output)
	sanitizer := masker.NewLogMasker()

	l := &Logger{
		lg:         &lg,
		basic:      structs.NewLogContent(sanitizer, headersLogList, types.ProductionProfile),
		sanitizer:  sanitizer,
		logHeaders: headersLogList,
		skipLevel:  defaultSkipLevel,
	}
	return l.Develop(), nil
}

/*
NewLogger - standard logger initialization function
*/
func NewLogger() *Logger {
	lg := zerolog.New(os.Stdout)
	sanitizer := masker.NewLogMasker()

	l := &Logger{
		lg:         &lg,
		basic:      structs.NewLogContent(sanitizer, headersLogList, types.ProductionProfile),
		sanitizer:  sanitizer,
		logHeaders: headersLogList,
		skipLevel:  defaultSkipLevel,
	}

	return l.Develop()
}

/*
NewAdvancedLogger - advanced logger initialization function. Allows to create logger with log lvl and pretty
print option
*/
func NewAdvancedLogger(logLevel string, pretty bool) *Logger {
	lvl, err := zerolog.ParseLevel(logLevel)
	if err != nil {
		fmt.Printf("failed to parse log lvl cfg: %v; set default - debug\n", err)
	}

	var output io.Writer = os.Stdout
	if pretty {
		output = zerolog.ConsoleWriter{Out: output, TimeFormat: time.RFC3339}
	}

	lg := zerolog.New(output).Level(lvl)
	sanitizer := masker.NewLogMasker()

	l := &Logger{
		lg:        &lg,
		basic:     structs.NewLogContent(sanitizer, headersLogList, types.ProductionProfile),
		sanitizer: sanitizer,
		skipLevel: defaultSkipLevel,
	}

	return l.Develop()
}

/*
Production sets a production profile & mode for the logger instance.
It affects the display of SQL queries.
*/
func (l *Logger) Production() *Logger {
	l.mode = productionMode
	l.basic.SetProfile(types.ProductionProfile)
	return l
}

/*
Develop sets develop profile & develop mode for logger.
*/
func (l *Logger) Develop() *Logger {
	l.mode = developMode
	l.basic.SetProfile(types.DevelopProfile)

	return l
}

/*
WithLogMask configures the logger to mask the values of certain fields.

Example:

	lg := logger.NewLogger().WithLogMask(&masker.MaskField{
		Field: "password",
		Mask: func(s string) string {
			return "*****"
		},
	})

	example := "password"
	value := "zorro"

	lg.Info().WithFields(field.StringField(example, value)).Printf("some message: %s", example)

will return:
{"level":"info","custom":{"password":"*****"}...."level":"Info","message":"some message: password"}
*/
func (l *Logger) WithLogMask(filters ...*masker.MaskField) *Logger {
	l.sanitizer.Append(filters...)
	return l
}

/*
WithCallerSkipLevel configures logger for correct caller's filename printing inside of logs
Default SkipLevel is 2
Log message with SkipLevel = 2
Example:

l.WithCallerSkipLevel(2).Print("hallo")

will return:
{"level":"info","caller":"folder/you_file.go:44","custom":{},"level":"Info","message":"hallo"}

Log message with SkipLevel = 3
Example:

l.WithCallerSkipLevel(2).Print("hallo")

will return:
{"level":"info","caller":"folder/main.go:44","custom":{},"level":"Info","message":"hallo"}
*/
func (l *Logger) WithCallerSkipLevel(lvl int) *Logger {
	l.skipLevel = lvl
	return l
}

/*
WithServiceName sets service name for Logger.basic.ServiceName.

Example:

	lg := logger.NewLogger().WithServiceName("my_awesome_service")
*/
func (l *Logger) WithServiceName(serviceName string) *Logger {
	l.basic.ServiceName = serviceName
	return l
}

func (l *Logger) WithOverridedHeadersLogList(headers ...string) *Logger {
	logHeader := make(map[string]struct{})

	for _, header := range headers {
		logHeader[header] = struct{}{}
	}

	l.logHeaders = logHeader
	l.basic.SetLogHeaders(l.logHeaders)
	return l
}

func (l *Logger) WithAuthorizationHeader() *Logger {
	l.logHeaders["Authorization"] = struct{}{}
	return l
}

/*
Info sets Logger.basic.Level as Info, sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Info() *Logger {
	lg := l.clone()
	lg.basic.Level = types.InfoLevel
	lg.basic.Date = time.Now()
	return lg
}

/*
Infof prints message with INFO log level
*/
func (l *Logger) Infof(msg string, args ...interface{}) {
	l.Info().Printf(msg, args...)
}

/*
Debug sets Logger.basic.Level as Debug if Logger.mode != production, else - sets level as Ignore,
also sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Debug() *Logger {
	lg := l.clone()

	if l.mode == productionMode {
		lg.basic.Level = types.IgnoreLevel
	} else {
		lg.basic.Level = types.DebugLevel
	}

	lg.basic.Date = time.Now()
	return lg
}

/*
Debugf prints message with DEBUG log level
*/
func (l *Logger) Debugf(msg string, args ...interface{}) {
	l.Debug().Printf(msg, args...)
}

/*
Error sets Logger.basic.Level as Error, sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Error() *Logger {
	lg := l.clone()
	lg.basic.Level = types.ErrorLevel
	lg.basic.Date = time.Now()
	return lg
}

/*
Errorf prints message with ERROR log level
*/
func (l *Logger) Errorf(msg string, args ...interface{}) {
	l.Error().Printf(msg, args...)
}

/*
Fatal sets Logger.basic.Level as Fatal, sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Fatal() *Logger {
	lg := l.clone()
	lg.basic.Level = types.FatalLevel
	lg.basic.Date = time.Now()
	return lg
}

/*
Fatalf prints message with FATAL log level
*/
func (l *Logger) Fatalf(msg string, args ...interface{}) {
	l.Fatal().Printf(msg, args...)
}

/*
Warn sets Logger.basic.Level as Warn, sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Warn() *Logger {
	lg := l.clone()
	lg.basic.Level = types.WarnLevel
	lg.basic.Date = time.Now()
	return lg
}

/*
Warnf prints message with WARN log level
*/
func (l *Logger) Warnf(msg string, args ...interface{}) {
	l.Warn().Printf(msg, args...)
}

/*
Trace sets Logger.basic.Level as Trace, sets time.Now() to Logger.basic.Date
*/
func (l *Logger) Trace() *Logger {
	lg := l.clone()
	lg.basic.Level = types.TraceLevel
	lg.basic.Date = time.Now()
	return lg
}

/*
Tracef prints message with TRACE log level
*/
func (l *Logger) Tracef(msg string, args ...interface{}) {
	l.Trace().Printf(msg, args...)
}

/*
WithInternalContext adds to Logger.basic.Context data from internal Context struct.
*/
func (l *Logger) WithInternalContext(ctx *loggerContext.Context) *Logger {
	lg := l.clone()
	lg.basic.Context = ctx
	return lg
}

/*
WithContext adds to Logger.basic.Context parsed data from context.Context.
It extracts RequestId, BsnOperationId, SystemID by keys from context. Also, it`s adding to Logger.basic.RequestId
extracted requestId
*/
func (l *Logger) WithContext(ctx context.Context) *Logger {
	lg := l.clone()
	reqId := extractIDKeyFromContext(ctx, loggerContext.RequestIDKey)

	lg.basic.Context = &loggerContext.Context{
		RequestId:      reqId,
		BsnOperationId: extractIDKeyFromContext(ctx, loggerContext.ProcessIDKey),
		SystemID:       extractIDKeyFromContext(ctx, loggerContext.SystemIDKey),
	}

	lg.basic.RequestId = reqId

	return lg
}

/*
WithFields adds a custom data to Logger.basic.Custom from Field structs.

Example:

	lg.Info().WithFields(field.StringField(example, value))
	lg.Info().WithFields(field.InterfaceField(example, value))
*/
func (l *Logger) WithFields(fields ...field.Field) *Logger {
	lg := l.clone()

	lg.basic.AddCustomContent(fields...)
	return lg
}

/*
WithError adds an error to Logger.basic.Custom and sets logger's verbalisation level to ERROR.

Example:

	err := errors.New("some err")
	lg.Info().WithError(err)
*/
func (l *Logger) WithError(err error) *Logger {
	lg := l.Error()

	lg.basic.AddCustomContent(field.ErrorField(err))
	return lg
}

/*
WithOptions adds options to Logger
*/
func (l *Logger) WithOptions(opts ...structs.LogContentOptions) *Logger {
	lg := l.clone()
	lg.basic.Apply(opts...)
	return lg
}

/*
Service sets to Logger.basic.Type value `SERVICE` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) Service(options ...structs.LogContentOptions) {
	l.basic.Type = types.MessageTypeService
	l.basic.Level = types.InfoLevel
	l.print(options...)
}

/*
UnhandledError sets to Logger.basic.Type value `ERROR` and sets to Logger.basic.Level - `Error`,
then - it calls print() method with passed options
*/
func (l *Logger) UnhandledError(options ...structs.LogContentOptions) {
	l.basic.Type = types.MessageTypeError
	l.basic.Level = types.ErrorLevel
	l.print(options...)
}

/*
HandledExceptionWarning sets to Logger.basic.Level - `Warn`,
then - it calls print() method with passed options
*/
func (l *Logger) HandledExceptionWarning(options ...structs.LogContentOptions) {
	l.basic.Level = types.WarnLevel
	l.print(options...)
}

/*
TraceInfo sets to Logger.basic.Type value `SERVICE` and sets to Logger.basic.Level - `Trace`,
then - it calls print() method with passed options
*/
func (l *Logger) TraceInfo(options ...structs.LogContentOptions) {
	l.basic.Level = types.TraceLevel
	l.basic.Type = types.MessageTypeService
	l.print(options...)
}

/*
BusinessLogicOperationStart calls logBusinessLogicInfo method with passed options
*/
func (l *Logger) BusinessLogicOperationStart(options ...structs.LogContentOptions) {
	l.logBusinessLogicInfo(options...)
}

/*
BusinessLogicOperationEnd calls logBusinessLogicInfo method with passed options
*/
func (l *Logger) BusinessLogicOperationEnd(options ...structs.LogContentOptions) {
	l.logBusinessLogicInfo(options...)
}

/*
BusinessLogicOperationError sets to Logger.basic.Type value `BL` and sets to Logger.basic.Level - `Error`,
then - it calls print() method with passed options
*/
func (l *Logger) BusinessLogicOperationError(options ...structs.LogContentOptions) {
	l.basic.Level = types.ErrorLevel
	l.basic.Type = types.MessageTypeBusinessLogic
	l.print(options...)
}

/*
BusinessActionStart calls logBusinessLogicInfo method with passed options
*/
func (l *Logger) BusinessActionStart(options ...structs.LogContentOptions) {
	l.logBusinessLogicInfo(options...)
}

/*
BusinessActionEnd calls logBusinessLogicInfo method with passed options
*/
func (l *Logger) BusinessActionEnd(options ...structs.LogContentOptions) {
	l.logBusinessLogicInfo(options...)
}

/*
BusinessActionError calls logBusinessLogicInfo method with passed options
*/
func (l *Logger) BusinessActionError(options ...structs.LogContentOptions) {
	l.logBusinessLogicInfo(options...)
}

/*
ApiRequest sets to Logger.basic.Type value `APIRQ` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) ApiRequest(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeApiRequest
	l.print(options...)
}

/*
ApiResponse sets to Logger.basic.Type value `APIRSP` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) ApiResponse(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeApiResponse
	l.print(options...)
}

/*
ExternalRequest sets to Logger.basic.Type value `EXTRQ` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) ExternalRequest(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeExternalRequest
	l.print(options...)
}

/*
ExternalRequestError sets to Logger.basic.Type value `EXTRQ` and sets to Logger.basic.Level - `Error`,
then - it calls print() method with passed options
*/
func (l *Logger) ExternalRequestError(options ...structs.LogContentOptions) {
	l.basic.Level = types.ErrorLevel
	l.basic.Type = types.MessageTypeExternalRequest
	l.print(options...)
}

/*
ExternalResponse sets to Logger.basic.Type value `EXTRSP` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) ExternalResponse(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeExternalResponse
	l.print(options...)
}

/*
EventPublish sets to Logger.basic.Type value `EVENTPUB` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) EventPublish(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeEventPub
	l.print(options...)
}

/*
EventPublishError sets to Logger.basic.Type value `EVENTPUB` and sets to Logger.basic.Level - `Error`,
then - it calls print() method with passed options
*/
func (l *Logger) EventPublishError(options ...structs.LogContentOptions) {
	l.basic.Level = types.ErrorLevel
	l.basic.Type = types.MessageTypeEventPub
	l.print(options...)
}

/*
DbRequestDebug - if Logger.mode is develop - method sets to Logger.basic.Type value `DBRQ` and
sets to Logger.basic.Level - `Debug`, then - it calls print() method with passed options
*/
func (l *Logger) DbRequestDebug(options ...structs.LogContentOptions) {
	if l.mode == developMode {
		l.basic.Level = types.DebugLevel
		l.basic.Type = types.MessageTypeDbRequest
		l.print(options...)
	}
}

/*
DbRequestInfo sets to Logger.basic.Type value `DBRQ` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) DbRequestInfo(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeDbRequest
	l.print(options...)
}

/*
DbRequestError sets to Logger.basic.Type value `DBRQ` and sets to Logger.basic.Level - `Error`,
then - it calls print() method with passed options
*/
func (l *Logger) DbRequestError(options ...structs.LogContentOptions) {
	l.basic.Level = types.ErrorLevel
	l.basic.Type = types.MessageTypeDbRequest
	l.print(options...)
}

/*
Print prints message with all Logger.Content, after that Logger calls Reset() method
*/
func (l *Logger) Print(msg string) {
	sourcefile := getSourceFile(l.skipLevel)
	l.basic.SetSourceFile(sourcefile)
	l.basic.WithDate(time.Now())

	fields := l.basic.Map()

	maps.DeleteFunc(fields, func(k string, v any) bool {
		return v == ""
	})

	if ev := l.event(); ev != nil {
		if l.mode == developMode && l.basic.Level == types.TraceLevel {
			// Extends message with trace stack. It's unavailable for Production env
			msg = fmt.Sprintf("%s. \nTrace %s", msg, debug.Stack())
		}
		ev.Fields(fields).Msg(msg)
	}

	for k := range fields {
		delete(fields, k)
	}
	l.basic.Reset()
}

/*
Printf prints message with args and with all Logger.Content, after that Logger calls
Logger.basic.Reset() method
*/
func (l *Logger) Printf(msg string, args ...interface{}) {
	sourcefile := getSourceFile(l.skipLevel)
	l.basic.SetSourceFile(sourcefile)
	l.basic.WithDate(time.Now())
	fields := l.basic.Map()

	maps.DeleteFunc(fields, func(k string, v any) bool {
		return v == ""
	})

	if ev := l.event(); ev != nil {
		if l.mode == developMode && l.basic.Level == types.TraceLevel {
			// Extends message with trace stack. It's unavailable for Production env
			msg = fmt.Sprintf("%s. \nTrace %s", msg, debug.Stack())
		}
		ev.Fields(fields).Msgf(msg, args...)
	}

	for k := range fields {
		delete(fields, k)
	}
	l.basic.Reset()
}

/*
logBusinessLogicInfo sets to Logger.basic.Type value `BL` and sets to Logger.basic.Level - `Info`,
then - it calls print() method with passed options
*/
func (l *Logger) logBusinessLogicInfo(options ...structs.LogContentOptions) {
	l.basic.Level = types.InfoLevel
	l.basic.Type = types.MessageTypeBusinessLogic
	l.print(options...)
}

/*
clone - method clones Logger with all Content
*/
func (l *Logger) clone() *Logger {
	nw := *l
	return &nw
}

/*
event returns zerolog Event by Logger.basic.Level, returns Info() by default
*/
func (l *Logger) event() *zerolog.Event {
	switch l.basic.Level {
	case types.InfoLevel:
		return l.lg.Info()
	case types.WarnLevel:
		return l.lg.Warn()
	case types.DebugLevel:
		return l.lg.Debug()
	case types.ErrorLevel:
		return l.lg.Error()
	case types.FatalLevel:
		return l.lg.Fatal()
	case types.TraceLevel:
		return l.lg.Trace()
	case types.IgnoreLevel:
		return nil
	default:
		return l.lg.Info()
	}
}

/*
print clones Logger, apply passed options by Logger.basic.Apply() method and prints log message based on
event() method result. After that - cleans fields by delete() method and than - calls Logger.basic.Reset()
*/
func (l *Logger) print(options ...structs.LogContentOptions) {
	lg := l.clone()

	sourcefile := getSourceFile(l.skipLevel)
	lg.basic.SetSourceFile(sourcefile)
	lg.basic.WithDate(time.Now())
	lg.basic.Apply(options...)

	fields := lg.basic.Map()

	maps.DeleteFunc(fields, func(k string, v any) bool {
		return v == ""
	})

	bytes, err := json.Marshal(fields)
	if err != nil {
		fmt.Println("logger Print marshal err: ", err)
	}
	var data map[string]any
	if err = json.Unmarshal(bytes, &data); err != nil {
		fmt.Println("logger Print unmarshal err: ", err)
	}

	if ev := lg.event(); ev != nil {
		ev.Fields(data).Send()
	}

	// Clean fields for GC
	for k := range fields {
		delete(fields, k)
	}
	lg.basic.Reset()
}

func extractIDKeyFromContext(ctx context.Context, key loggerContext.IDKey) string {
	val := ctx.Value(key)

	if val == nil {
		return ""
	}
	return fmt.Sprintf("%v", val)
}

func getSourceFile(skipLevel int) string {
	_, filename, line, ok := runtime.Caller(skipLevel)
	if !ok {
		return ""
	}

	idx := strings.LastIndexByte(filename, '/')
	if idx == -1 {
		return filename
	}
	idx = strings.LastIndexByte(filename[:idx], '/')
	if idx == -1 {
		return filename
	}
	return fmt.Sprintf("%s:%d", filename[idx+1:], line)
}
