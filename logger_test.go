package logger

import (
	"bytes"
	"encoding/json"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/danilpan/arm-logger/masker"
	"gitlab.com/danilpan/arm-logger/structs"
)

func TestNewLogger(t *testing.T) {
	l := NewLogger()
	assert.NotNil(t, l)
}

func TestLogger_ApiRequest(t *testing.T) {
	asserts := assert.New(t)
	out := &bytes.Buffer{}

	t.Run("single call with default masks", func(t *testing.T) {
		lg, err := NewLoggerWithOutput(out)
		asserts.Nil(err)
		asserts.NotNil(lg)

		lg.ApiRequest(structs.ApiRequestOptions(
			HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
		))

		var result map[string]any
		err = json.Unmarshal(out.Bytes(), &result)
		require.NoError(t, err)

		for key, value := range ApiRequestMap {
			mappedData, ok := result[key]
			asserts.True(ok)
			asserts.Equal(mappedData, value)
		}

		out.Reset()
	})

	t.Run("single call with custom masks", func(t *testing.T) {
		lg, err := NewLoggerWithOutput(out)
		lg = lg.WithLogMask(
			&masker.MaskField{
				Field: "password",
				Mask: func(s string) string {
					return "*********"
				},
			},
		)
		asserts.Nil(err)
		asserts.NotNil(lg)

		lg.ApiRequest(structs.ApiRequestOptions(
			HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
		),
			structs.CustomStringOptions("password", "Qwerty123"),
		)

		var result map[string]any
		err = json.Unmarshal(out.Bytes(), &result)
		require.NoError(t, err)

		for key, value := range ApiRequestMap {
			mappedData, ok := result[key]
			asserts.True(ok)
			asserts.Equal(mappedData, value)
		}

		out.Reset()
	})
}

func TestLogger_Highload(t *testing.T) {
	asserts := assert.New(t)
	lg := NewLogger().WithLogMask(maskFields...)
	asserts.NotNil(lg)
	wg := sync.WaitGroup{}

	start := time.Now()

	for i := 0; i < 1000; i++ {
		wg.Add(10)
		go func() {
			lg.ApiRequest(structs.ApiRequestOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
			)
			wg.Done()
		}()
		go func() {
			lg.ApiResponse(structs.ApiResponseOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
				structs.CustomStringOptions("card_number", "1234"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.ExternalCallOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
				structs.CustomStringOptions("phone", "+90(223)998-09-00-34"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.MessageQueueOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams,
			),
				structs.CustomStringOptions("bin", "223123123"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.MetricsOptions(
				HttpMethodGet, time.Duration(200),
			),
				structs.CustomStringOptions("iin", "223123123"),
			)
			wg.Done()
		}()
		go func() {
			lg.ApiRequest(structs.ApiRequestOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
			)
			wg.Done()
		}()
		go func() {
			lg.ApiResponse(structs.ApiResponseOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
				structs.CustomStringOptions("card_number", "1234"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.ExternalCallOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams,
			),
				structs.CustomStringOptions("password", "Aa1232123"),
				structs.CustomStringOptions("user", "Sam Smith"),
				structs.CustomStringOptions("phone", "+90(223)998-09-00-34"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.MessageQueueOptions(
				HttpMethodGet, HttpApiVersion, HttpApiQueryParams,
			),
				structs.CustomStringOptions("bin", "223123123"),
			)
			wg.Done()
		}()
		go func() {
			lg.ExternalRequest(structs.MetricsOptions(
				HttpMethodGet, time.Duration(200),
			),
				structs.CustomStringOptions("iin", "223123123"),
			)
			wg.Done()
		}()
	}
	wg.Wait()
	end := time.Now()
	asserts.True((end.UnixMilli() - start.UnixMilli()) < 3000)
}

func BenchmarkLogger(b *testing.B) {
	lg := NewLogger().WithLogMask(
		&masker.MaskField{
			Field: "password",
			Mask: func(s string) string {
				return "*********"
			},
		},
		&masker.MaskField{
			Field: "user",
			Mask: func(s string) string {
				return "Vasiliy Malkov"
			},
		},
		&masker.MaskField{
			Field: "card_number",
			Mask: func(s string) string {
				return "0000-0000-0000-0000"
			},
		},
	)
	lg.ApiResponse(structs.ApiResponseOptions(HttpMethodGet, HttpApiVersion, HttpApiQueryParams, requestId),
		structs.CustomStringOptions("password", "Aa1232123"),
		structs.CustomStringOptions("user", "Sam Smith"),
		structs.CustomStringOptions("card_number", "1234"),
		structs.CustomStringOptions("password1", "Aa1232123"),
		structs.CustomStringOptions("user1", "Sam Smith"),
		structs.CustomStringOptions("card_number1", "1234"),
		structs.CustomStringOptions("password2", "Aa1232123"),
		structs.CustomStringOptions("user2", "Sam Smith"),
		structs.CustomStringOptions("card_number2", "1234"),
		structs.CustomStringOptions("password3", "Aa1232123"),
		structs.CustomStringOptions("user3", "Sam Smith"),
		structs.CustomStringOptions("card_number3", "1234"),
		structs.CustomOptions("interface", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface2", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface3", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface4", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface5", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface6", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomOptions("interface7", map[string]interface{}{
			"some_field": "some_value",
		}),
		structs.CustomStructOptions("struct", struct {
			Value string `json:"value"`
		}{
			Value: "SOME_VALUE",
		}),
		structs.CustomStructOptions("struct1", struct {
			Value1 string `json:"value1"`
		}{
			Value1: "SOME_VALUE",
		}),
		structs.CustomStructOptions("struct2", struct {
			Value2 string `json:"value2"`
		}{
			Value2: "SOME_VALUE",
		}),
		structs.CustomStructOptions("struct3", struct {
			Value3 string `json:"value3"`
		}{
			Value3: "SOME_VALUE",
		}),
	)

	b.ReportAllocs()
}
