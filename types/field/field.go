package field

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"time"
)

type fieldType string

const (
	StringFieldType    fieldType = "string"
	IntFieldType       fieldType = "int"
	Int16FieldType     fieldType = "int16"
	Int32FieldType     fieldType = "int32"
	Int64FieldType     fieldType = "int64"
	Float32FieldType   fieldType = "float32"
	Float64FieldType   fieldType = "float64"
	BoolFieldType      fieldType = "bool"
	StructFieldType    fieldType = "struct"
	MapFieldType       fieldType = "map"
	InterfaceFieldType fieldType = "interface"
	DurationFieldType  fieldType = "duration"
	TimeFieldType      fieldType = "time"
	ErrorFieldType     fieldType = "error"
)

var mapping = map[fieldType]reflect.Type{
	StringFieldType:  reflect.TypeOf(""),
	IntFieldType:     reflect.TypeOf(int(1)),
	Int16FieldType:   reflect.TypeOf(int16(1)),
	Int32FieldType:   reflect.TypeOf(int32(1)),
	Int64FieldType:   reflect.TypeOf(int64(1)),
	Float32FieldType: reflect.TypeOf(float32(1.1)),
	Float64FieldType: reflect.TypeOf(float64(1.1)),
	BoolFieldType:    reflect.TypeOf(false),
	ErrorFieldType:   reflect.TypeOf(errors.New("")),
}

type Field struct {
	key   string
	value interface{}
	_type fieldType
}

func (f Field) Key() string {
	return f.key
}

func (f Field) Interface() interface{} {
	return f.value
}

func (f Field) IsString() bool {
	return f.canTransform(mapping[StringFieldType])
}

func (f Field) canTransform(t reflect.Type) bool {
	t1 := mapping[f._type]
	if t1 == nil {
		return false
	}
	return t1.ConvertibleTo(t)
}

func (f Field) AsInt() int {
	if f.canTransform(mapping[IntFieldType]) {
		return f.value.(int)
	}
	return 0
}

func (f Field) AsInt16() int16 {
	if f.canTransform(mapping[Int16FieldType]) {
		return f.value.(int16)
	}
	return 0
}

func (f Field) AsInt32() int32 {
	if f.canTransform(mapping[Int32FieldType]) {
		return f.value.(int32)
	}
	return 0
}

func (f Field) AsInt64() int64 {
	if f.canTransform(mapping[Int64FieldType]) {
		return f.value.(int64)
	}
	return 0
}

func (f Field) AsFloat32() float32 {
	if f.canTransform(mapping[Float32FieldType]) {
		return f.value.(float32)
	}
	return 0
}

func (f Field) AsFloat64() float64 {
	if f.canTransform(mapping[Float64FieldType]) {
		return f.value.(float64)
	}
	return 0
}

func (f Field) AsBool() bool {
	if f.canTransform(mapping[BoolFieldType]) {
		return f.value.(bool)
	}
	return false
}

func (f Field) AsString() string {
	switch f._type {
	case StringFieldType:
		return f.value.(string)
	case IntFieldType, Int16FieldType, Int32FieldType, Int64FieldType:
		return strconv.Itoa(f.AsInt())
	case Float64FieldType:
		return fmt.Sprintf("%f", f.AsFloat64())
	case Float32FieldType:
		return fmt.Sprintf("%f", f.AsFloat32())
	case BoolFieldType:
		bv := f.AsBool()
		if bv {
			return "true"
		} else {
			return "false"
		}
	case StructFieldType, MapFieldType:
		bts, _ := json.Marshal(f.value)
		return string(bts)
	case InterfaceFieldType:
		return fmt.Sprintf("%v", f.value)
	case ErrorFieldType:
		return f.value.(string)
	default:
		return ""
	}
}
func (f Field) Type() string {
	return string(f._type)
}

func StringField(key string, value string) Field {
	return newField(key, value, StringFieldType)
}

func DurationField(key string, d time.Duration) Field {
	return newField(key, d, DurationFieldType)
}

func TimeField(key string, t time.Time) Field {
	return newField(key, t, TimeFieldType)
}

func IntField(key string, value int) Field {
	return newField(key, value, IntFieldType)
}

func Int16Field(key string, value int16) Field {
	return newField(key, value, Int16FieldType)
}

func Int32Field(key string, value int32) Field {
	return newField(key, value, Int32FieldType)
}

func Int64Field(key string, value int64) Field {
	return newField(key, value, Int64FieldType)
}

func Float32Field(key string, value float32) Field {
	return newField(key, value, Float32FieldType)
}

func Float64Field(key string, value float64) Field {
	return newField(key, value, Float64FieldType)
}

func BooleanField(key string, value bool) Field {
	return newField(key, value, BoolFieldType)
}

func StructField(key string, value interface{}) Field {
	return newField(key, value, StructFieldType)
}

func MapField(key string, value map[string]interface{}) Field {
	return newField(key, value, MapFieldType)
}

func InterfaceField(key string, value interface{}) Field {
	return newField(key, value, InterfaceFieldType)
}

func ErrorField(err error) Field {
	return newField("error", err.Error(), ErrorFieldType)
}

func newField(key string, value interface{}, tp fieldType) Field {
	return Field{
		key:   key,
		value: value,
		_type: tp,
	}
}
