package masker

import "sync"

type Mask func(s string) string

type MaskField struct {
	Field string
	Mask  Mask
}

type LogMasker interface {
	Append(filters ...*MaskField)
	Apply(key, value string) string
}

func NewLogMasker() LogMasker {
	return &logMasker{
		mu:     sync.Mutex{},
		fields: map[string]Mask{},
	}
}

type logMasker struct {
	mu     sync.Mutex
	fields map[string]Mask
}

func (l *logMasker) Append(filters ...*MaskField) {
	var ok bool
	l.mu.Lock()
	for _, v := range filters {
		if _, ok = l.fields[v.Field]; !ok {
			l.fields[v.Field] = v.Mask
		}
	}
	l.mu.Unlock()
}

func (l *logMasker) Apply(key, value string) string {
	if mask, ok := l.fields[key]; ok {
		return mask(value)
	}
	return value
}
