package echo

import (
	"context"
	"errors"
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/danilpan/arm-logger"
	"gitlab.com/danilpan/arm-logger/structs"
	loggerContext "gitlab.com/danilpan/arm-logger/structs/context"
)

type RequestsLogMiddleware struct {
	lg          *logger.Logger
	serviceName string
}

func NewRequestsLogMiddleware(l *logger.Logger, serviceName string) *RequestsLogMiddleware {
	return &RequestsLogMiddleware{
		lg:          l,
		serviceName: serviceName,
	}
}

func (l *RequestsLogMiddleware) Handle(c echo.Context) error {
	l.lg.ApiRequest(structs.EchoApiRequestOptions(c))
	return nil
}

func (l *RequestsLogMiddleware) HandleEchoLogger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		r := c.Request()

		if r.URL.String() == "/ready" || r.URL.String() == "/live" {
			return next(c)
		}

		requestID := r.Header.Get(echo.HeaderXRequestID)

		if requestID == "" {
			requestID = uuid.New().String()
			r.Header.Set(echo.HeaderXRequestID, requestID)
		}

		ctx := context.WithValue(r.Context(), loggerContext.RequestIDKey, requestID)

		c.SetRequest(r.WithContext(ctx))

		c.Response().Header().Add(echo.HeaderXRequestID, requestID)

		l.lg.Info().ApiRequest(structs.EchoApiRequestOptions(c))

		err := next(c)

		if err != nil && errors.Is(echo.ErrNotFound, err) {
			c.Response().Status = http.StatusNotFound
		}

		l.lg.Info().ApiResponse(structs.EchoApiResponseOptions(c))

		return err
	}
}
