package fiber

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v3"
	fiberlog "github.com/gofiber/fiber/v3/middleware/logger"
	"github.com/gofiber/fiber/v3/middleware/requestid"
	"gitlab.com/danilpan/arm-logger"
	"gitlab.com/danilpan/arm-logger/structs"
)

type LogMiddleware struct {
	lg *logger.Logger
}

func NewRequestsLogMiddleware(l *logger.Logger) *LogMiddleware {
	return &LogMiddleware{
		lg: l,
	}
}

func (r *LogMiddleware) Handle() fiber.Handler {
	return func(c fiber.Ctx) error {
		r.lg.ApiRequest(structs.FiberApiRequestOptions(c))
		err := c.Next()

		if err != nil {
			var e *fiber.Error
			c.Response().SetStatusCode(fiber.StatusInternalServerError)
			if errors.As(err, &e) {
				c.Response().SetStatusCode(e.Code)
			}

			r.lg.ExternalRequestError(structs.FiberApiResponseOptions(c))
			return err
		}
		r.lg.ApiResponse(structs.FiberApiResponseOptions(c))
		return err
	}
}

func (r *LogMiddleware) RequestId() fiber.Handler {
	return requestid.New(requestid.ConfigDefault)
}

func (r *LogMiddleware) FiberLoger() fiber.Handler {
	return fiberlog.New(fiberlog.Config{
		Format: fmt.Sprintln("======== fiber =======\ndate: ${time} | method: ${method} | headers: ${reqHeaders}",
			"| path: ${path} | params: ${queryParams} | bodyRequest: ${body} | status: ${status}",
			"| response: ${resBody} | latency: ${latency} | requestId: ${reqHeader:X-Request-ID}",
			"| error: ${error}\n====== fiber end ====="),
		TimeFormat: "02.01.2006 07:04:05",
		Done: func(c fiber.Ctx, logString []byte) {
			if c.Response().StatusCode() == fiber.StatusInternalServerError {
				next := struct {
					RequestId string `json:"requestId"`
					ErrorMsg  string `json:"msg"`
				}{ErrorMsg: string(c.Response().Body()), RequestId: requestid.FromContext(c)}
				_ = c.Status(fiber.StatusInternalServerError).JSON(next)
			}
		},
	})
}
