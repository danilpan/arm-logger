package gin

import (
	g "github.com/gin-gonic/gin"
	"gitlab.com/danilpan/arm-logger"
	"gitlab.com/danilpan/arm-logger/structs"
)

type RequestsLogMiddleware struct {
	lg *logger.Logger
}

func NewRequestsLogMiddleware(l *logger.Logger) *RequestsLogMiddleware {
	return &RequestsLogMiddleware{
		lg: l,
	}
}

func (r *RequestsLogMiddleware) Handle(c *g.Context) {
	r.lg.ApiRequest(structs.GinApiRequestOptions(c))
	c.Next()
}
