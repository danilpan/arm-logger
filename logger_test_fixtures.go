package logger

import (
	"fmt"
	"net/http"

	"gitlab.com/danilpan/arm-logger/masker"
)

// Fixtures

// variables
const (
	HttpMethodGet        = http.MethodGet
	requestId            = "efd46eaa-c6b0-4be4-8f84-a80de9821fba"
	HttpApiVersion       = "1.0.0"
	HttpApiQueryParams   = "?name=Tom&lastname=Hanks"
	AriRqLogString       = "{\"level\":\"info\",\"api_request\":{\"api_method\":\"GET\",\"api_params\":\"?name=Tom\\u0026lastname=Hanks\",\"api_version\":\"1.0.0\"},\"custom\":{},\"date\":\"0001-01-01T00:00:00Z\",\"level\":\"Info\",\"request_id\":\"efd46eaa-c6b0-4be4-8f84-a80de9821fba\",\"service_name\":\"\",\"system\":\"\",\"type\":\"APIRQ\"}\n"
	AriRqLogMaskedString = "{\"level\":\"info\",\"api_request\":{\"api_method\":\"GET\",\"api_params\":\"?name=Tom\\u0026lastname=Hanks\",\"api_version\":\"1.0.0\"},\"custom\":{\"password\":\"*********\"},\"date\":\"0001-01-01T00:00:00Z\",\"level\":\"Info\",\"request_id\":\"efd46eaa-c6b0-4be4-8f84-a80de9821fba\",\"service_name\":\"\",\"system\":\"\",\"type\":\"APIRQ\"}\n"
)

var (
	// ApiRequestMap aborting time check - it always different
	ApiRequestMap = map[string]any{
		"level": "info",
		"api_request": map[string]any{
			"api_method":  "GET",
			"api_params":  "?name=Tom&lastname=Hanks",
			"api_version": "1.0.0",
		},
		"request_id": "efd46eaa-c6b0-4be4-8f84-a80de9821fba",
		"type":       "APIRQ",
	}

	ApiRequestMaskedMap = map[string]any{
		"level": "Info",
		"api_request": map[string]any{
			"api_method":  "GET",
			"api_params":  "?name=Tom&lastname=Hanks",
			"api_version": "1.0.0",
			"password":    "*********",
		},
		"request_id":   "efd46eaa-c6b0-4be4-8f84-a80de9821fba",
		"service_name": "",
		"system":       "",
		"type":         "APIRQ",
	}
)

// TestCases
var (
	maskFields = []*masker.MaskField{
		{
			Field: "password",
			Mask: func(s string) string {
				return "*********"
			},
		},
		{
			Field: "card_number",
			Mask: func(s string) string {
				if len(s) < 4 {
					return "****-****-****-****"
				}
				return fmt.Sprintf("%s-xxxx-xxxx-xxxx", s[:4])
			},
		},
		{
			Field: "phone",
			Mask: func(s string) string {
				return "+7(xxx)xxx-xx-xx"
			},
		},
		{
			Field: "user",
			Mask: func(s string) string {
				return "John Malkov"
			},
		},
		{
			Field: "bin",
			Mask: func(s string) string {
				return "0987654321"
			},
		},
		{
			Field: "iin",
			Mask: func(s string) string {
				return "0987654321"
			},
		},
	}
)
