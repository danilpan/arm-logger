package external

type ExternalCall struct {
	ExtSystem    string `json:"ExtSystem"`
	ExtRequestId string `json:"ExtRequestId"`
	ExtMethod    string `json:"ExtMethod"`
}
