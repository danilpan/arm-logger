package queue

type MessageQueue struct {
	MqQueueName string `json:"MqQueueName"`
	MqOperation string `json:"MqOperation"`
	MqMessage   string `json:"MqMessage"`
}
