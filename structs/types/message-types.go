package types

type MessageType string

const (
	MessageTypeService          MessageType = "SERVICE"
	MessageTypeBusinessLogic    MessageType = "BL"
	MessageTypeError            MessageType = "ERROR"
	MessageTypeApiRequest       MessageType = "APIRQ"
	MessageTypeApiResponse      MessageType = "APIRSP"
	MessageTypeExternalRequest  MessageType = "EXTRQ"
	MessageTypeExternalResponse MessageType = "EXTRSP"
	MessageTypeDbRequest        MessageType = "DBRQ"
	MessageTypeEventPub         MessageType = "EVENTPUB"
)
