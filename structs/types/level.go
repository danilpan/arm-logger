package types

type LogLevel string
type Profile string

const (
	TraceLevel  LogLevel = "Trace"
	DebugLevel  LogLevel = "Debug"
	WarnLevel   LogLevel = "Warn"
	InfoLevel   LogLevel = "Info"
	ErrorLevel  LogLevel = "Error"
	FatalLevel  LogLevel = "Fatal"
	IgnoreLevel LogLevel = "ignore"
)

const (
	DevelopProfile    Profile = "dev"
	ProductionProfile Profile = "prod"
)
