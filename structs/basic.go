package structs

import (
	"strconv"
	"time"

	"gitlab.com/danilpan/arm-logger/masker"
	"gitlab.com/danilpan/arm-logger/structs/api"
	"gitlab.com/danilpan/arm-logger/structs/business"
	internalContext "gitlab.com/danilpan/arm-logger/structs/context"
	"gitlab.com/danilpan/arm-logger/structs/exceptions"
	"gitlab.com/danilpan/arm-logger/structs/external"
	"gitlab.com/danilpan/arm-logger/structs/http"
	"gitlab.com/danilpan/arm-logger/structs/metrics"
	"gitlab.com/danilpan/arm-logger/structs/queue"
	"gitlab.com/danilpan/arm-logger/structs/sql"
	"gitlab.com/danilpan/arm-logger/structs/sys"
	"gitlab.com/danilpan/arm-logger/structs/trace"
	"gitlab.com/danilpan/arm-logger/structs/types"
	"gitlab.com/danilpan/arm-logger/types/field"
)

type LogContentOptions func(l *LogContent)

type LogContent struct {
	profile    types.Profile
	sanitizer  masker.LogMasker
	logHeaders map[string]struct{}
	Date       time.Time         `json:"Date"`
	Level      types.LogLevel    `json:"Level"`
	Type       types.MessageType `json:"Type"`
	// Conditional value for unique identification of an event by the monitoring service
	SourceFile             string                           `json:"caller"`
	System                 string                           `json:"System"`
	RequestId              string                           `json:"RequestId"`
	Text                   string                           `json:"Text"`
	ServiceName            string                           `json:"ServiceName"`
	Version                string                           `json:"Version"`
	Marker                 string                           `json:"Marker"`                // ??
	Context                *internalContext.Context         `json:"Context,omitempty"`     // internal context struct
	ApiRequest             *api.ApiRequest                  `json:"ApiRequest,omitempty"`  // external API request data
	ApiResponse            *api.ApiResponse                 `json:"ApiResponse,omitempty"` // external API response data
	BusinessLogicOperation *business.BusinessLogicOperation `json:"BusinessLogicOperation,omitempty"`
	// Software exception. This block should only be used in combination with the ERROR or WARNING level
	Exception *exceptions.Exception `json:"Exception,omitempty"`
	// Calling an external system. In combination with the INFO level,
	// it means the call was completed successfully, and in combination with the ERROR level, the call made an error.
	ExternalCall *external.ExternalCall `json:"ExternalCall,omitempty"`
	// Request via HTTP protocol. Can be used in combination with an external API call.
	HttpIncomingRequest *http.HttpIncomingRequest `json:"HttpIncomingRequest,omitempty"`
	HttpResponse        *http.HttpResponse        `json:"HttpResponse,omitempty"`
	HttpRequest         *http.HttpRequest         `json:"HttpRequest,omitempty"`
	MessageQueue        *queue.MessageQueue       `json:"MessageQueue,omitempty"`
	Metrics             []metrics.Metrics         `json:"Metrics,omitempty"`
	// Information for audit, filled in case of logging information audit events if available
	SysInfo    *sys.SysInfo    `json:"SysInfo,omitempty"`
	SqlRequest *sql.SqlRequest `json:"SqlRequest,omitempty"`
	Trace      *trace.Trace    `json:"Trace,omitempty"`
	Custom     *Node           `json:"Custom,omitempty"`
}

func NewLogContent(sanitizer masker.LogMasker, logHeaders map[string]struct{}, prof types.Profile) LogContent {
	return LogContent{
		sanitizer:  sanitizer,
		logHeaders: logHeaders,
		Custom: &Node{
			sanitizer: sanitizer,
			Data:      &field.Field{},
		},
		profile: prof,
	}
}

func (l *LogContent) AddCustomContent(fields ...field.Field) {
	var (
		workNode *Node
		nextNode *Node
	)
	workNode = l.Custom
	for _, v := range fields {
		workNode.Data = &v
		nextNode = &Node{
			Parent:    workNode,
			sanitizer: l.sanitizer,
			Data:      &field.Field{},
		}
		workNode.Next = nextNode
		workNode = nextNode
	}
	l.Custom = workNode
}

func (l *LogContent) SetProfile(prof types.Profile) {
	l.profile = prof
}

func (l *LogContent) WithDate(date time.Time) {
	l.Date = date
}

func (l *LogContent) SetSanitizer(sanitizer masker.LogMasker) {
	l.sanitizer = sanitizer
}

func (l *LogContent) SetLogHeaders(logHeaders map[string]struct{}) {
	l.logHeaders = logHeaders
}

func (l *LogContent) Map() map[string]any {

	m := map[string]any{
		"date": l.Date.Format(time.RFC3339),
	}

	if value, isEmpty := checkString(l.SourceFile); !isEmpty {
		m["caller"] = value
	}

	if value, isEmpty := checkString(string(l.Type)); !isEmpty {
		m["type"] = value
	}

	if value, isEmpty := checkString(l.RequestId); !isEmpty {
		m["request_id"] = value
	}

	if value, isEmpty := checkString(l.ServiceName); !isEmpty {
		m["service_name"] = value
	}

	if value, isEmpty := checkString(l.System); !isEmpty {
		m["system"] = value
	}

	if value, isEmpty := checkString(l.Text); !isEmpty {
		m["text"] = value
	}

	if value, isEmpty := checkString(l.Version); !isEmpty {
		m["version"] = value
	}

	if value, isEmpty := checkString(l.Marker); !isEmpty {
		m["marker"] = value
	}

	if l.Context != nil {
		m["context"] = prepareContext(l.Context)
	}

	if l.ApiRequest != nil {
		m["api_request"] = prepareApiRequest(l.ApiRequest)
	}

	if l.ApiResponse != nil {
		m["api_response"] = prepareApiResponse(l.ApiResponse)
	}

	if l.BusinessLogicOperation != nil {
		m["business_logic_operation"] = prepareBusinessOperationData(l.BusinessLogicOperation)
	}

	if l.Exception != nil {
		m["exception"] = prepareExceptionData(l.Exception)
	}

	if l.ExternalCall != nil {
		m["external_call"] = prepareExternalCallData(l.ExternalCall)
	}

	if l.HttpIncomingRequest != nil {
		m["http_incoming_request"] = prepareIncRequestData(l.HttpIncomingRequest)
	}

	if l.HttpRequest != nil {
		headers := make(map[string]string, len(l.HttpRequest.HttpHeaders))
		for k, v := range l.HttpRequest.HttpHeaders {
			_, ok := l.logHeaders[k]
			if ok && l.sanitizer != nil {
				headers[k] = l.sanitizer.Apply(k, v)
			} else if ok {
				headers[k] = v
			}
		}

		l.HttpRequest.HttpHeaders = headers

		m["http_request"] = prepareRequestData(l.HttpRequest)
	}

	if l.HttpResponse != nil {
		m["http_response"] = prepareResponseData(l.HttpResponse)
	}

	if l.MessageQueue != nil {
		m["message_queue"] = prepareMsgQueueData(l.MessageQueue)
	}

	if l.SysInfo != nil {
		m["sys_info"] = prepareSysInfoData(l.SysInfo)
	}

	if l.SqlRequest != nil && l.profile != types.ProductionProfile {
		m["sql_request"] = prepareSqlReqData(l.SqlRequest)
	}

	if l.Metrics != nil && len(l.Metrics) > 0 {
		mtr := map[string]any{}
		for _, v := range l.Metrics {
			mtr[v.MtrName] = v.MtrDurationNsec
		}
		m["metrics"] = mtr
	}

	if l.Custom != nil {
		customData := l.Custom.Map()
		if customData != nil {
			m["custom"] = customData
		}
	}

	return m
}

func (l *LogContent) Apply(opts ...LogContentOptions) {
	for _, opt := range opts {
		opt(l)
	}
}

// Reset cleans up transitive objects for GC
func (l *LogContent) Reset() {
	var (
		node *Node
		next *Node
	)

	l.Context = nil
	l.ApiResponse = nil
	l.ApiResponse = nil
	l.HttpResponse = nil
	l.BusinessLogicOperation = nil
	l.Exception = nil
	l.ExternalCall = nil
	l.Trace = nil
	l.HttpIncomingRequest = nil
	l.HttpRequest = nil
	l.Metrics = nil
	l.MessageQueue = nil
	l.SysInfo = nil
	l.SqlRequest = nil

	node = l.Custom
	for node != nil {
		node.Parent = nil
		next = node.Next
		node.Next = nil
		node = next
	}

	l.Custom = nil
}

func (l *LogContent) SetSourceFile(sourcefile string) {
	l.SourceFile = sourcefile
}

func prepareContext(context *internalContext.Context) map[string]any {
	contextData := make(map[string]any)

	if value, isEmpty := checkString(context.RequestId); !isEmpty {
		contextData["request_id"] = value
	}

	if value, isEmpty := checkString(context.BsnOperationId); !isEmpty {
		contextData["bsn_operation_id"] = value
	}

	if value, isEmpty := checkString(context.BsnOperation); !isEmpty {
		contextData["bsn_operation"] = value
	}

	if value, isEmpty := checkString(context.BsnUser); !isEmpty {
		contextData["bsn_user"] = value
	}

	if value, isEmpty := checkString(context.SystemID); !isEmpty {
		contextData["system_id"] = value
	}

	return contextData
}

func prepareApiRequest(apiReq *api.ApiRequest) map[string]any {
	apiRequestData := make(map[string]any)

	if value, isEmpty := checkString(apiReq.ApiMethod); !isEmpty {
		apiRequestData["api_method"] = value
	}

	if value, isEmpty := checkString(apiReq.ApiVersion); !isEmpty {
		apiRequestData["api_version"] = value
	}

	if value, isEmpty := checkString(apiReq.ApiParameters); !isEmpty {
		apiRequestData["api_params"] = value
	}

	if value, isEmpty := checkString(apiReq.ApiURL); !isEmpty {
		apiRequestData["api_url"] = value
	}

	return apiRequestData
}

func prepareApiResponse(resp *api.ApiResponse) map[string]any {
	apiRespData := make(map[string]any)

	if value, isEmpty := checkString(resp.ApiMethod); !isEmpty {
		apiRespData["api_method"] = value
	}

	if value, isEmpty := checkString(resp.ApiVersion); !isEmpty {
		apiRespData["api_version"] = value
	}

	if value, isEmpty := checkString(resp.ApiResult); !isEmpty {
		apiRespData["api_result"] = value
	}

	if value, isEmpty := checkString(strconv.Itoa(resp.ApiStatusCode)); !isEmpty {
		apiRespData["api_status_code"] = value
	}

	if value, isEmpty := checkString(resp.ApiURL); !isEmpty {
		apiRespData["api_url"] = value
	}

	return apiRespData
}

func prepareBusinessOperationData(b *business.BusinessLogicOperation) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(b.BsnOperationId); !isEmpty {
		data["bsn_operation_id"] = value
	}

	if value, isEmpty := checkString(b.BsnOperation); !isEmpty {
		data["bsn_operation"] = value
	}

	if value, isEmpty := checkString(b.BsnUser); !isEmpty {
		data["bsn_user"] = value
	}

	if value, isEmpty := checkString(b.BsnOperationDetails); !isEmpty {
		data["bsn_operation_details"] = value
	}

	if value, isEmpty := checkString(b.BsnAction); !isEmpty {
		data["bsn_action"] = value
	}

	return data
}

func prepareExceptionData(e *exceptions.Exception) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(e.ExMessage); !isEmpty {
		data["ex_message"] = value
	}

	if value, isEmpty := checkString(e.ExStackTrace); !isEmpty {
		data["ex_stack_trace"] = value
	}

	if value, isEmpty := checkString(e.ExInnerExceptions); !isEmpty {
		data["ex_inner_exception"] = value
	}

	if value, isEmpty := checkString(string(e.ExType)); !isEmpty {
		data["ex_type"] = value
	}

	if value, isEmpty := checkString(e.ExMessageExpanded); !isEmpty {
		data["ex_message_expanded"] = value
	}

	return data
}

func prepareExternalCallData(e *external.ExternalCall) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(e.ExtSystem); !isEmpty {
		data["ext_system"] = value
	}

	if value, isEmpty := checkString(e.ExtRequestId); !isEmpty {
		data["ext_request_id"] = value
	}

	if value, isEmpty := checkString(e.ExtMethod); !isEmpty {
		data["ext_method"] = value
	}

	return data
}

func prepareIncRequestData(h *http.HttpIncomingRequest) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(h.HttpRequest.HttpMethod); !isEmpty {
		data["http_method"] = value
	}

	if value, isEmpty := checkString(h.HttpRequest.HttpUri); !isEmpty {
		data["uri"] = value
	}

	if value, isEmpty := checkString(h.HttpRequest.HttpQueryParams); !isEmpty {
		data["http_query_params"] = value
	}

	if h.HttpRequest.HttpHeaders != nil {
		data["http_headers"] = h.HttpRequest.HttpHeaders
	}

	if value, isEmpty := checkString(h.HttpRequest.HttpBody); !isEmpty {
		data["http_body"] = value
	}

	if value, isEmpty := checkString(h.HttpClientAddress); !isEmpty {
		data["http_client_addr"] = value
	}

	return data
}

func prepareRequestData(h *http.HttpRequest) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(h.HttpMethod); !isEmpty {
		data["http_method"] = value
	}

	if value, isEmpty := checkString(h.HttpUri); !isEmpty {
		data["uri"] = value
	}

	if value, isEmpty := checkString(h.HttpQueryParams); !isEmpty {
		data["http_query_params"] = value
	}

	if h.HttpHeaders != nil {
		data["http_headers"] = h.HttpHeaders
	}

	if value, isEmpty := checkString(h.HttpBody); !isEmpty {
		data["http_body"] = value
	}

	return data
}

func prepareResponseData(r *http.HttpResponse) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(r.HttpMethod); !isEmpty {
		data["http_method"] = value
	}

	if value, isEmpty := checkString(r.HttpUri); !isEmpty {
		data["http_uri"] = value
	}

	if value, isEmpty := checkString(r.HttpStatusCode); !isEmpty {
		data["http_status_code"] = value
	}

	if value, isEmpty := checkString(r.HttpReasonPhrase); !isEmpty {
		data["http_reason_phrase"] = value
	}

	if value, isEmpty := checkString(r.HttpContent); !isEmpty {
		data["http_content"] = value
	}

	if value, isEmpty := checkString(r.HttpContentType); !isEmpty {
		data["http_content_type"] = value
	}

	return data
}

func prepareMsgQueueData(q *queue.MessageQueue) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(q.MqQueueName); !isEmpty {
		data["queue_name"] = value
	}

	if value, isEmpty := checkString(q.MqOperation); !isEmpty {
		data["queue_operation"] = value
	}

	if value, isEmpty := checkString(q.MqMessage); !isEmpty {
		data["message"] = value
	}

	return data
}

func prepareSysInfoData(s *sys.SysInfo) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(s.FromClientIP); !isEmpty {
		data["from_client_ip"] = value
	}

	if value, isEmpty := checkString(s.ToClientIP); !isEmpty {
		data["to_client_ip"] = value
	}

	if value, isEmpty := checkString(s.FromClientName); !isEmpty {
		data["from_client_name"] = value
	}

	return data
}

func prepareSqlReqData(s *sql.SqlRequest) map[string]any {
	data := make(map[string]any)

	if value, isEmpty := checkString(s.Sql); !isEmpty {
		data["sql"] = value
	}

	if value, isEmpty := checkString(s.SqlParameters); !isEmpty {
		data["params"] = value
	}

	return data
}

func checkString(data string) (string, bool) {
	var isEmpty bool
	if data == "" {
		isEmpty = true
	}

	return data, isEmpty
}
