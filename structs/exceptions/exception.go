package exceptions

type Exception struct {
	ExMessage         string        `json:"ExMessage"`
	ExStackTrace      string        `json:"ExStackTrace"`
	ExInnerExceptions string        `json:"ExInnerExceptions"`
	ExType            ExceptionType `json:"ExType"`
	ExMessageExpanded string        `json:"ExMessageExpanded"`
}
