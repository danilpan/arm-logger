package exceptions

// Exception types

type ExceptionType string

const (
	LowPriorException  ExceptionType = "low"
	MidPriorException  ExceptionType = "medium"
	HighPriorException ExceptionType = "high"
)
