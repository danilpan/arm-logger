package sql

type SqlRequest struct {
	Sql           string `json:"Sql"`
	SqlParameters string `json:"SqlParameters"`
}
