package api

type ApiResponse struct {
	ApiMethod     string `json:"ApiMethod"`
	ApiVersion    string `json:"ApiVersion"`
	ApiResult     string `json:"ApiResult,omitempty"`
	ApiStatusCode int    `json:"ApiStatusCode"`
	ApiURL        string `json:"ApiURL"`
}
