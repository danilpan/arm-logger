package api

type ApiRequest struct {
	ApiMethod     string `json:"ApiMethod"`
	ApiVersion    string `json:"ApiVersion"`
	ApiParameters string `json:"ApiParameters,omitempty"`
	ApiURL        string `json:"ApiURL"`
}
