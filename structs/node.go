package structs

import (
	"gitlab.com/danilpan/arm-logger/masker"
	"gitlab.com/danilpan/arm-logger/types/field"
)

type Node struct {
	Parent    *Node
	Next      *Node
	Data      *field.Field
	sanitizer masker.LogMasker
}

func (n *Node) Map() map[string]interface{} {
	var (
		key  string
		s    string
		val  interface{}
		node *Node
	)
	m := map[string]interface{}{}
	node = n

	for node != nil {
		if key = node.Data.Key(); key != "" {
			if node.Data.IsString() {
				s = node.Data.AsString()
				m[key] = node.sanitizer.Apply(key, s)
			} else {
				if val = node.Data.Interface(); val != nil {
					m[key] = val
				}
			}
		}
		node = node.Parent
	}
	if len(m) != 0 {
		return m
	}

	return nil
}
