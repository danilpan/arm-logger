package http

type HttpResponse struct {
	HttpMethod       string `json:"HttpMethod"`
	HttpUri          string `json:"HttpUri"`
	HttpStatusCode   string `json:"HttpStatusCode"`
	HttpReasonPhrase string `json:"HttpReasonPhrase"`
	HttpContent      string `json:"HttpContent"`
	HttpContentType  string `json:"HttpContentType"`
}
