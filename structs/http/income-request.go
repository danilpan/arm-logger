package http

type HttpIncomingRequest struct {
	HttpRequest
	HttpClientAddress string `json:"HttpClientAddress"`
}
