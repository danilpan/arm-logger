package http

type HttpRequest struct {
	HttpMethod      string            `json:"HttpMethod"`
	HttpUri         string            `json:"HttpUri"`
	HttpQueryParams string            `json:"HttpQueryParams"`
	HttpHeaders     map[string]string `json:"HttpHeaders"`
	HttpBody        string            `json:"HttpBody"`
}
