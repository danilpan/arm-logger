package sys

type SysInfo struct {
	FromClientIP   string `json:"FromClientIP"`
	ToClientIP     string `json:"ToClientIP"`
	FromClientName string `json:"FromClientName"`
}
