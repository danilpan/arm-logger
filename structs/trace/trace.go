package trace

type Trace struct {
	TrcCallerBase string `json:"TrcCallerBase"`
	TrcMethod     string `json:"TrcMethod"`
	TrcData       string `json:"TrcData"`
	TrcPosition   string `json:"TrcPosition"`
}
