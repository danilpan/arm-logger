package context

const (
	RequestIDKey IDKey = "x-request-id"
	SystemIDKey  IDKey = "x-system-id"
	ProcessIDKey IDKey = "x-process-id"
)

type (
	Context struct {
		RequestId      string      `json:"RequestId"`
		BsnOperation   string      `json:"BsnOperation"`
		BsnOperationId string      `json:"BsnOperationId,omitempty"`
		BsnUser        string      `json:"BsnUser"`
		SystemID       string      `json:"system_id,omitempty"`
		Result         interface{} `json:"result,omitempty"`
	}
	IDKey string
)
