package business

type BusinessLogicOperation struct {
	BsnOperation        string `json:"BsnOperation"`
	BsnAction           string `json:"BsnAction"`
	BsnOperationId      string `json:"BsnOperationId"`
	BsnOperationDetails string `json:"BsnOperationDetails"`
	BsnUser             string `json:"BsnUser"`
}
