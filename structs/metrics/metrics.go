package metrics

type Metrics struct {
	MtrName         string `json:"MtrName"`
	MtrDurationNsec int64  `json:"MtrDurationNsec"`
}
