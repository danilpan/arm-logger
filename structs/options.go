package structs

import (
	"fmt"
	http2 "net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gofiber/fiber/v3"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"github.com/gofiber/fiber/v3/middleware/requestid"
	"gitlab.com/danilpan/arm-logger/structs/api"
	"gitlab.com/danilpan/arm-logger/structs/business"
	"gitlab.com/danilpan/arm-logger/structs/context"
	"gitlab.com/danilpan/arm-logger/structs/exceptions"
	"gitlab.com/danilpan/arm-logger/structs/external"
	"gitlab.com/danilpan/arm-logger/structs/http"
	"gitlab.com/danilpan/arm-logger/structs/metrics"
	"gitlab.com/danilpan/arm-logger/structs/queue"
	"gitlab.com/danilpan/arm-logger/structs/sql"
	"gitlab.com/danilpan/arm-logger/structs/sys"
	"gitlab.com/danilpan/arm-logger/types/field"
)

func ContextOptions(requestId, bsnOperation, bsnOperationId, bsnUser string) LogContentOptions {
	ctx := &context.Context{
		RequestId:      requestId,
		BsnOperation:   bsnOperation,
		BsnOperationId: bsnOperationId,
		BsnUser:        bsnUser,
	}
	return func(l *LogContent) {
		l.Context = ctx
	}
}

func GinApiRequestOptions(c *gin.Context) LogContentOptions {
	return HttpApiRequestOptions(c.Request)
}

func EchoApiRequestOptions(c echo.Context) LogContentOptions {
	return HttpRequestOptions(c.Request())
}

func EchoApiResponseOptions(c echo.Context) LogContentOptions {
	return HttpApiResponseOptions(c)
}

func FiberApiRequestOptions(c fiber.Ctx) LogContentOptions {
	return httpFiberRequestOptions(c)
}

func FiberApiResponseOptions(c fiber.Ctx) LogContentOptions {
	return httpFiberResponseOptions(c)
}

func HttpApiRequestOptions(r *http2.Request) LogContentOptions {
	requestID := r.Header.Get(echo.HeaderXRequestID)

	if requestID == "" {
		requestID = uuid.New().String()
		r.Header.Set(echo.HeaderXRequestID, requestID)
	}

	result := &api.ApiRequest{
		ApiMethod:     r.Method,
		ApiVersion:    getVersion(r.URL.Path),
		ApiParameters: r.URL.Query().Encode(),
		ApiURL:        r.RequestURI,
	}

	return func(l *LogContent) {
		l.ApiRequest = result
		l.RequestId = requestID
		l.Date = time.Now()
	}
}

func HttpApiResponseOptions(c echo.Context) LogContentOptions {
	r := c.Response()

	rsp := &http.HttpResponse{
		HttpMethod:      c.Request().Method,
		HttpUri:         c.Request().RequestURI,
		HttpContentType: r.Header().Get(echo.HeaderContentType),
		HttpStatusCode:  strconv.Itoa(c.Response().Status),
	}

	return func(l *LogContent) {
		l.HttpResponse = rsp
	}
}

func ApiRequestOptions(method, version, queryParams, requestId string) LogContentOptions {
	r := &api.ApiRequest{
		ApiMethod:     method,
		ApiVersion:    version,
		ApiParameters: queryParams,
	}

	return func(l *LogContent) {
		l.ApiRequest = r
		l.RequestId = requestId
		l.Date = time.Now()
	}
}

func ApiResponseOptions(method, version, result, requestId string) LogContentOptions {
	r := &api.ApiResponse{
		ApiMethod:  method,
		ApiVersion: version,
		ApiResult:  result,
	}
	return func(l *LogContent) {
		l.ApiResponse = r
		l.RequestId = requestId
		l.Date = time.Now()
	}
}

func BusinessLogicOperationOptions(operation, operationId, operationDetails, action, user string) LogContentOptions {
	bsn := &business.BusinessLogicOperation{
		BsnOperation:        operation,
		BsnAction:           action,
		BsnOperationId:      operationId,
		BsnOperationDetails: operationDetails,
		BsnUser:             user,
	}
	return func(l *LogContent) {
		l.BusinessLogicOperation = bsn
	}
}

func ExceptionOptions(message string, exType exceptions.ExceptionType, err error) LogContentOptions {
	e := &exceptions.Exception{
		ExMessage: message,
		ExType:    exType,
	}
	if err != nil {
		e.ExMessageExpanded = err.Error()
	}
	return func(l *LogContent) {
		l.Exception = e
	}
}

func ExternalCallOptions(system, requestId, method string) LogContentOptions {
	ec := &external.ExternalCall{
		ExtMethod:    method,
		ExtSystem:    system,
		ExtRequestId: requestId,
	}

	return func(l *LogContent) {
		l.ExternalCall = ec
	}
}

func GinHttpIncomingRequestOptions(c *gin.Context) LogContentOptions {
	return HttpIncomingRequestOptions(c.Request)
}

func EchoHttpIncomingRequestOptions(c echo.Context) LogContentOptions {
	return HttpIncomingRequestOptions(c.Request())
}

func HttpIncomingRequestOptions(r *http2.Request) LogContentOptions {
	hrq := httpRequestFromObj(r)
	req := &http.HttpIncomingRequest{
		HttpRequest:       *hrq,
		HttpClientAddress: r.RemoteAddr,
	}

	return func(l *LogContent) {
		l.HttpIncomingRequest = req
	}
}

func GinHttpRequestOptions(c *gin.Context) LogContentOptions {
	return HttpRequestOptions(c.Request)
}

func EchoHttpRequestOptions(c echo.Context) LogContentOptions {
	return HttpRequestOptions(c.Request())
}

func HttpRequestOptions(r *http2.Request) LogContentOptions {
	req := httpRequestFromObj(r)

	return func(l *LogContent) {
		l.HttpRequest = req
	}
}

func httpRequestFromObj(r *http2.Request) *http.HttpRequest {
	req := &http.HttpRequest{}
	req.HttpHeaders = map[string]string{}
	for k, v := range r.Header {
		req.HttpHeaders[k] = strings.Join(v, "|")
	}
	req.HttpQueryParams = r.URL.Query().Encode()
	req.HttpMethod = r.Method
	req.HttpUri = r.URL.String()

	return req
}

func HttpResponseOptions(method, uri, content, contentType, reason string, statusCode int) LogContentOptions {
	rsp := &http.HttpResponse{
		HttpMethod:       method,
		HttpUri:          uri,
		HttpContent:      content,
		HttpContentType:  contentType,
		HttpStatusCode:   strconv.Itoa(statusCode),
		HttpReasonPhrase: reason,
	}
	return func(l *LogContent) {
		l.HttpResponse = rsp
	}
}

func MessageQueueOptions(topic, operation, msg string) LogContentOptions {
	mq := &queue.MessageQueue{
		MqMessage:   msg,
		MqOperation: operation,
		MqQueueName: topic,
	}
	return func(l *LogContent) {
		l.MessageQueue = mq
	}
}

// MetricsOptions sets new metric to your log. Be careful, duration should be provided in nanoseconds
func MetricsOptions(name string, duration time.Duration) LogContentOptions {
	m := metrics.Metrics{
		MtrName:         name,
		MtrDurationNsec: int64(duration),
	}
	return func(l *LogContent) {
		if l.Metrics == nil {
			l.Metrics = []metrics.Metrics{m}
		} else {
			l.Metrics = append(l.Metrics, m)
		}
	}
}

func SysInfoOptions(fromIp, toIp, clientName string) LogContentOptions {
	s := &sys.SysInfo{
		FromClientIP:   fromIp,
		ToClientIP:     toIp,
		FromClientName: clientName,
	}
	return func(l *LogContent) {
		l.SysInfo = s
	}
}

func SqlRequestOptions(query string, params ...interface{}) LogContentOptions {
	s := &sql.SqlRequest{
		Sql:           query,
		SqlParameters: fmt.Sprintf("%v", params...),
	}
	return func(l *LogContent) {
		l.SqlRequest = s
	}
}

func CustomOptions(key string, value interface{}) LogContentOptions {
	return func(l *LogContent) {
		l.AddCustomContent(field.InterfaceField(key, value))
	}
}

func CustomStringOptions(key string, value string) LogContentOptions {
	return func(l *LogContent) {
		l.AddCustomContent(field.StringField(key, value))
	}
}

func CustomStructOptions(key string, value interface{}) LogContentOptions {
	return func(l *LogContent) {
		l.AddCustomContent(field.StructField(key, value))
	}
}

func getVersion(path string) string {
	rgx := regexp.MustCompile(`/v\d+/`)
	return rgx.FindString(path)
}

func httpFiberRequestOptions(c fiber.Ctx) LogContentOptions {
	requestID := c.Get(echo.HeaderXRequestID)

	if requestID == "" {
		requestID = requestid.FromContext(c)
		if requestID != "" {
			c.Request().Header.Set(fiber.HeaderXRequestID, requestID)
		}
	}

	result := &api.ApiRequest{
		ApiMethod:     c.Method(),
		ApiVersion:    getVersion(c.Route().Path),
		ApiParameters: string(c.Request().URI().QueryString()),
		ApiURL:        c.Request().URI().String(),
	}

	return func(l *LogContent) {
		l.ApiRequest = result
		l.RequestId = requestID
		l.Date = time.Now()
	}
}

func httpFiberResponseOptions(c fiber.Ctx) LogContentOptions {
	requestID := c.Get(echo.HeaderXRequestID)

	if requestID == "" {
		requestID = requestid.FromContext(c)
		c.Request().Header.Set(fiber.HeaderXRequestID, requestID)
	}

	result := &api.ApiResponse{
		ApiMethod:     c.Method(),
		ApiVersion:    getVersion(c.Route().Path),
		ApiStatusCode: c.Response().StatusCode(),
		//ApiURL:        c.Request().URI().String(),
	}

	return func(l *LogContent) {
		l.ApiResponse = result
		l.RequestId = requestID
		l.Date = time.Now()
	}
}
